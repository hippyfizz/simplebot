import logging
import os

import hypercorn.asyncio
from quart import Quart, session

from client import client
from routes import (
    arena_handler,
    hatches_handler,
    main_menu_handler,
    reports_handler,
    root_handler,
    spam_briliant_handler,
    spam_car_steal_handler,
    spam_hatch_handler,
)
from tasks import clan_war_task

logger = logging.getLogger(__name__)
app = Quart(__name__)
app.add_url_rule(rule="/arena", view_func=arena_handler, methods=["POST"])
app.add_url_rule(rule="/hatches", view_func=hatches_handler, methods=["POST"])
app.add_url_rule(rule="/reports", view_func=reports_handler, methods=["POST"])
app.add_url_rule(
    rule="/main_menu", view_func=main_menu_handler, methods=["GET", "POST"]
)
app.add_url_rule(
    rule="/spam_briliant", view_func=spam_briliant_handler, methods=["GET", "POST"]
)
app.add_url_rule(
    rule="/spam_car_steal", view_func=spam_car_steal_handler, methods=["GET", "POST"]
)
app.add_url_rule(
    rule="/spam_hatch", view_func=spam_hatch_handler, methods=["GET", "POST"]
)

app.add_url_rule(rule="/", view_func=root_handler, methods=["GET", "POST"])
# app.secret_key = uuid.uuid4().hex
app.secret_key = "e3efe5ac027b4fd39ec57a4ea5a98b0a"


# Connect the client before we start serving with Quart
@app.before_serving
async def startup():
    await client.connect()
    app.add_background_task(clan_war_task)


@app.before_request
def make_session_permanent():
    # session.clear()
    session.permanent = True


# After we're done serving (near shutdown), clean up the client
@app.after_serving
async def cleanup():
    await client.disconnect()


async def main():
    config = hypercorn.Config()
    host = os.environ.get("HOST", "0.0.0.0")
    port = os.environ.get("PORT", 80)
    config.bind = f"{host}:{port}"
    await hypercorn.asyncio.serve(app, config)


if __name__ == "__main__":
    client.loop.run_until_complete(main())
