from datetime import datetime
from typing import Optional

from pydantic import BaseModel, Field

from vars import State

options = None


class Options(BaseModel):
    is_config_loaded: bool = Field(default=False)
    arena: bool = Field(default=False)
    hatches: bool = Field(default=False)
    reports: bool = Field(default=True)
    state: State = Field(default=State.PENDING)
    phone: Optional[str]
    last_report_sent_at: Optional[datetime]


def get_options():
    global options
    if options is None:
        options = Options()
    return options


__all__ = ["get_options"]
