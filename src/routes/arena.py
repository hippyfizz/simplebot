import asyncio
from datetime import timedelta

from quart import redirect, session, url_for

from client import client
from options import get_options
from vars import State, clan_warsbot_username


async def arena_handler():
    options = get_options()
    if not options.arena:
        options.arena = True
        options.hatches = False
        if options.state == options.state.HATCHES:
            await client.send_message(clan_warsbot_username, "Отменить приключение❌")
            await asyncio.sleep(timedelta(seconds=1).total_seconds())
        options.state = State.ARENA
        await client.send_message(clan_warsbot_username, "⚔️Арена")
    else:
        options.arena = False
        options.state = State.PENDING
    session["arena"] = options.arena
    session["hatches"] = options.hatches
    session["state"] = options.state.value
    return redirect(url_for("main_menu_handler"))
