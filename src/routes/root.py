from quart import redirect, render_template, request, url_for, session
from telethon.errors import SessionPasswordNeededError

from client import client
from options import get_options
from vars import State


async def root_handler():
    # We want to update the global phone variable to remember it
    options = get_options()
    # Check form parameters (phone/code)
    form = await request.form
    if "phone" in form:
        options.phone = form["phone"]
        await client.send_code_request(options.phone)

    if "code" in form:
        try:
            await client.sign_in(code=form["code"])
        except SessionPasswordNeededError:
            return await render_template(
                "base.html", content=await render_template("password_form.html")
            )

    if "password" in form:
        await client.sign_in(password=form["password"])

    # If we're logged in, show them some messages from their first dialog
    if await client.is_user_authorized():
        # They are logged in, show them main menu
        return redirect(url_for("main_menu_handler"))

    # Ask for the phone if we don't know it yet
    if options.phone is None:
        return await render_template(
            "base.html", content=await render_template("phone_form.html")
        )

    # We have the phone, but we're not logged in, so ask for the code
    return await render_template(
        "base.html", content=await render_template("code_form.html")
    )
