from quart import redirect, session, url_for

from options import get_options


async def reports_handler():
    options = get_options()
    if options.reports is True:
        options.reports = False
    else:
        options.reports = True
    session["reports"] = options.reports
    return redirect(url_for("main_menu_handler"))
