import asyncio

from quart import redirect, url_for
from telethon.tl import functions
from telethon.tl.types import PeerUser

from client import client


async def spam_hatch_handler():
    full = await client(functions.users.GetFullUserRequest(PeerUser(2063668248)))
    message = await client.get_messages("clan_warsbot", ids=full.pinned_msg_id)
    while True:
        await asyncio.sleep(1.2)
        await message.buttons[0][0].click()
        await asyncio.sleep(1.2)
        await client.send_message("clan_warsbot", "👻 Кража люков (15 минут)")
    return redirect(url_for("main_menu_handler"))
