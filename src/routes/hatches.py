from quart import redirect, session, url_for

from client import client
from options import get_options
from vars import State, clan_warsbot_username


async def hatches_handler():
    options = get_options()
    if not options.hatches:
        options.hatches = True
        options.arena = False
        options.state = State.HATCHES
        await client.send_message(clan_warsbot_username, "👻 Кража люков (15 минут)")
    else:
        options.hatches = False
        options.state = State.PENDING
    session["arena"] = options.arena
    session["hatches"] = options.hatches
    session["state"] = options.state.value
    return redirect(url_for("main_menu_handler"))
