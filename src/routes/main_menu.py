import logging

from quart import render_template, session

from options import get_options
from utils import load_config
from vars import State

logger = logging.getLogger('quart.serving')


async def main_menu_handler():
    load_config()
    options = get_options()
    if options.last_report_sent_at:
        session["last_report_sent_at"] = options.last_report_sent_at
    last_report_sent_at_formatted = (
        options.last_report_sent_at.strftime("%d.%m.%Y %H:%M:%S")
        if options.last_report_sent_at
        else None
    )
    return await render_template(
        "base.html",
        content=await render_template(
            "main_menu.html",
            arena_value="Выключить" if options.arena else "Включить",
            hatches_value="Выключить" if options.hatches else "Включить",
            report_value="Выключить" if options.reports else "Включить",
            last_report_sent_at=last_report_sent_at_formatted,
        ),
    )
