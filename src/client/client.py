import os

from telethon import TelegramClient

from utils import get_env

# Session name, API ID and hash to use; loaded from environmental variables

SESSION = os.environ.get("TG_SESSION", "quart")
API_ID = int(get_env("TG_API_ID", "Enter your API ID: "))
API_HASH = get_env("TG_API_HASH", "Enter your API hash: ")
# _client = TelegramClient(
#     f"{SESSION}", API_ID, API_HASH
# )

_client = TelegramClient(
    f"{os.environ.get('SESSIONS_DIR')}/{SESSION}", API_ID, API_HASH
)
_client.parse_mode = "html"  # <- Render things nicely
