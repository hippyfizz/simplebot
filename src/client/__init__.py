from telethon import events

from client.client import _client
from client.handlers import handle_clan_warsbot_message, handle_squad_bot_command
from vars import clan_warsbot_username, cw_potato_bot_user_id, squad_channel_id

_client.add_event_handler(
    handle_clan_warsbot_message,
    events.NewMessage(incoming=True, from_users=[clan_warsbot_username]),
)
_client.add_event_handler(
    handle_squad_bot_command,
    events.NewMessage(
        incoming=True,
        chats=[squad_channel_id],
        from_users=[cw_potato_bot_user_id],
    ),
)
client = _client
__all__ = ["client"]
