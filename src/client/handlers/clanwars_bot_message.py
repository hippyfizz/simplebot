import asyncio
from datetime import timedelta, datetime

import pytz

from client.client import _client
from options import get_options
from vars import clan_warsbot_username, squad_channel_id


async def handle_clan_warsbot_message(event):
    options = get_options()
    if options.arena:
        if "⚔️Ход сражения" in event.message.message:
            return
        elif "Вы сражаетесь на арене⚔️" in event.message.message:
            return
        elif (
            "☹️Вы проиграли!" in event.message.message
            or "🎉Вы победили!" in event.message.message
        ):
            await _client.send_message(clan_warsbot_username, "⚔️Арена")
            return
        elif "⚔️Сразитесь против других игроков" in event.message.message:
            await event.message.click(text="Найти соперника")
            return
        elif "⚔️Поиск противника запущен" in event.message.message:
            return
        elif "⚔️Поиск противника отменён" in event.message.message:
            await _client.send_message(clan_warsbot_username, "⚔️Арена")
            return
    elif options.hatches:
        if (
            "🚓Приключение провалено" in event.message.message
            or "успешно завершено" in event.message.message
        ):
            await _client.send_message(
                clan_warsbot_username, "👻 Кража люков (15 минут)"
            )
            return
    if "Ваши личные результаты" in event.message.message:
        await _client.forward_messages(squad_channel_id, event.message)
        options.last_report_sent_at = pytz.timezone("Europe/Moscow").localize(datetime.now())
        await asyncio.sleep(timedelta(seconds=5).total_seconds())
        if options.arena:
            await _client.send_message(clan_warsbot_username, "⚔️Арена")
            return
        elif options.hatches:
            await _client.send_message(
                clan_warsbot_username, "👻 Кража люков (15 минут)"
            )
            return
