import asyncio

from telethon.tl.functions.messages import SendInlineBotResultRequest
from telethon.tl.types import UpdateNewMessage

from client.client import _client
from vars import clan_warsbot_username, cw_potato_bot_user_id


async def handle_squad_bot_command(event):
    try:
        query = event.message.reply_markup.rows[0].buttons[0].query
    except Exception:
        query = None
    if not query:
        return
    inline_results = await _client.inline_query(cw_potato_bot_user_id, query)
    for res in inline_results:
        response = await _client(
            SendInlineBotResultRequest(
                clan_warsbot_username,
                inline_results.query_id,
                res.result.id,
            )
        )
        await asyncio.sleep(2)
        for update in response.updates:
            if isinstance(update, UpdateNewMessage):
                message = await _client.get_messages(
                    clan_warsbot_username, ids=update.message.id
                )
                await message.click(text="Я прожался")
                break
