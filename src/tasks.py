import asyncio
import logging
from datetime import datetime, time, timedelta
from random import randint

import pytz

from client import client
from options import get_options
from vars import clan_warsbot_username, State

logger = logging.getLogger(__name__)


async def request_war_report_task():
    options = get_options()
    logger.info(f"Starting request_war_report_task")
    while True:
        if not options.reports:
            await asyncio.sleep(timedelta(minutes=1).total_seconds())
            continue
        tzdata = pytz.timezone("Europe/Moscow")
        now = datetime.now()
        now = tzdata.localize(now)
        today_war_end_date = now.combine(
            date=now.date(),
            time=time(
                22,
                randint(1, 3),
                randint(0, 59),
            ),
        )
        today_war_end_date = tzdata.localize(today_war_end_date)
        logger.info(f"{now} and {today_war_end_date}")
        if (
            options.last_report_sent_at is not None
            and options.last_report_sent_at.date() == now.date()
        ):
            pass
        elif now > today_war_end_date or not options.last_report_sent_at:
            if options.state != State.PENDING:
                await client.send_message(clan_warsbot_username, "Отменить приключение❌")
                await asyncio.sleep(timedelta(seconds=2).total_seconds())
            await client.send_message(clan_warsbot_username, "🎭Результаты войны")
            options.last_report_sent_at = datetime.now(pytz.timezone("Europe/Moscow"))
        await asyncio.sleep(timedelta(minutes=1).total_seconds())


async def clan_war_task():
    asyncio.get_event_loop().create_task(request_war_report_task())
