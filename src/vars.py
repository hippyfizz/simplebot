from datetime import time
from enum import Enum
from random import randint

clan_warsbot_username = "clan_warsbot"
squad_channel_id = 1689952452
cw_potato_bot_user_id = 2121609214
war_end_time = time(22, randint(1, 3), randint(0, 59))


class State(Enum):
    PENDING = "pending"
    ARENA = "arena"
    HATCHES = "hatches"
