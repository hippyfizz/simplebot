import os

from quart import session

from options import get_options
from vars import State


def get_env(name, message):
    if name in os.environ:
        return os.environ[name]
    return input(message)


def load_config():
    options = get_options()
    if options.is_config_loaded:
        return
    options.arena = session["arena"] if session.get("arena") is not None else False
    options.hatches = session["hatches"] if session.get("hatches") is not None else False
    options.reports = session["reports"] if session.get("reports") is not None else True
    options.state = State(session["state"]) if session.get("state") is not None else State.PENDING
    options.last_report_sent_at = session.get("last_report_sent_at")
    options.is_config_loaded = True
